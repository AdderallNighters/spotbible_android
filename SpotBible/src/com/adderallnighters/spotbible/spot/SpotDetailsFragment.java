package com.adderallnighters.spotbible.spot;


import java.util.UUID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.Image;
import com.adderallnighters.spotbible.model.Spot;
import com.adderallnighters.spotbible.model.SpotManager;
import com.adderallnighters.spotbible.utils.PictureUtils;

public class SpotDetailsFragment extends Fragment {

	private static final String TAG = SpotDetailsFragment.class.getSimpleName();
	public static final String EXTRA_SPOT_ID= "com.adderallnighters.spotbible.spot_id";
	private static final int REQUEST_IMAGE = 1;
	
	private Spot mSpot;
	private EditText mSpotName;
	private CheckBox mCapped;
	private ImageButton mPhotoButton;
	private ImageView mImageView;
	
	public static SpotDetailsFragment newInstance(UUID spotId)
	{
		Bundle args = new Bundle();
		args.putSerializable(EXTRA_SPOT_ID, spotId);
		SpotDetailsFragment fragment = new SpotDetailsFragment();
		fragment.setArguments(args);
		return fragment;
	}
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		UUID spotId = (UUID) getArguments().getSerializable(EXTRA_SPOT_ID);
		this.mSpot = SpotManager.get(getActivity()).getSpot(spotId);
	}
	
	@SuppressLint("NewApi")
	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_spot,parent, false);
		this.mSpotName = (EditText)view.findViewById(R.id.spot_name);
		this.mSpotName.setText(mSpot.getName());
		this.mSpotName.addTextChangedListener(new TextWatcher()
		{

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				mSpot.setName(s.toString());
				
			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
			
		});
		this.mPhotoButton = (ImageButton)view.findViewById(R.id.spot_imageButton);
		this.mPhotoButton.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, SpotDetailsFragment.REQUEST_IMAGE);
				startActivityForResult(intent, SpotDetailsFragment.REQUEST_IMAGE);
			}
		});
		
		this.mImageView = (ImageView) view.findViewById(R.id.spot_image_view);
		
		//If camera is not available, disable the button
		PackageManager pm = getActivity().getPackageManager();
		boolean hasCam = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) ||
				pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT) ||
				(Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && 
				Camera.getNumberOfCameras() > 0);
		if(!hasCam){
			mPhotoButton.setEnabled(false);
		}
		
		return view;
	}
	
	@Override 
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(resultCode != Activity.RESULT_OK) return;
		if(requestCode == REQUEST_IMAGE){

		}
	}
	
	@Override 
	public void onStart()
	{
		super.onStart();
		showImage();
	}
	
	@Override
	public void onStop()
	{
		super.onStop();
		PictureUtils.cleanImageView(mImageView);
	}
	
	private void showImage()
	{
		Image i = mSpot.getImage();
		BitmapDrawable b = null;
		if(i != null) {
			String path = getActivity().getFileStreamPath(i.getFilename()).getAbsolutePath();
			b = PictureUtils.getScaledDrawable(getActivity(), path);
		}
		mImageView.setImageDrawable(b);
	}
	
}
