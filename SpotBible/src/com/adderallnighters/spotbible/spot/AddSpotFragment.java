package com.adderallnighters.spotbible.spot;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.utils.MultipartEntity;

public class AddSpotFragment extends Fragment {
	private ImageView mImageView;
	private EditText mEditText;
	private Button mUploadButton;
	private Bitmap mImageToUpload;
	private static final int CAMERA = 0;
	private static final int GALLERY = 1;
	private static final String TAG = "upload";
	private static final String IMAGE_UPLOAD_URL = "http://li973-66.members.linode.com/text";
	private String userId;
	
	
	public AddSpotFragment(String userId) {
		this.userId = userId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, parent, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_add_spot, parent, false);
		mImageView = (ImageView) view.findViewById(R.id.image_view_spot_pic);
		mImageView.setImageResource(R.drawable.camera_normal);	
		mImageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showPhotoChoice();
			}
		});
		mEditText = (EditText) view.findViewById(R.id.spot_name);
		mUploadButton = (Button) view.findViewById(R.id.button_upload_spot);
		mUploadButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				try {
					sendPhoto(mImageToUpload);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
		});
		return view;
	}

	private void showPhotoChoice() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		CharSequence camera = getResources().getString(
				R.string.action_photo_camera);
		CharSequence gallery = getResources().getString(
				R.string.action_photo_gallery);
		builder.setCancelable(true).setItems(
				new CharSequence[] { camera, gallery },
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						if (i == CAMERA) {
							startCameraIntent();
						} else if (i == GALLERY) {
							startGalleryIntent();
						}
					}
				});
		builder.show();
	}

	private void startCameraIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		// Ensure that there's a camera activity to handle the intent
		if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
			// Create the File where the photo should go
			File photoFile = null;
			try {
				photoFile = createImageFile();
			} catch (IOException ex) {
				// Error occurred while creating the File

			}
			// Continue only if the File was successfully created
			if (photoFile != null) {
				takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(photoFile));
				startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
			}
		}
	}

	
	private void startGalleryIntent() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("image/*");
		String selectPicture = getResources().getString(R.string.select_picture);
		startActivityForResult(Intent.createChooser(intent, selectPicture),REQUEST_GET_PHOTO);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i(TAG, "onActivityResult: " + this);
		if (requestCode == REQUEST_TAKE_PHOTO && resultCode == Activity.RESULT_OK) {
			setPicCamera();
		}
		else if(requestCode == REQUEST_GET_PHOTO && resultCode == Activity.RESULT_OK){
			Uri selectedImage = data.getData();
			setPicGallery(selectedImage);
		}
		
	}

	private void sendPhoto(Bitmap bitmap) throws Exception {
		new UploadTask().execute(bitmap);
	}

	private class UploadTask extends AsyncTask<Bitmap, Void, Void> {

		protected Void doInBackground(Bitmap... bitmaps) {
			if (bitmaps[0] == null)
				return null;
			// setProgress(0);

			Bitmap bitmap = bitmaps[0];
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream); // convert
																		// Bitmap
																		// to
																		// ByteArrayOutputStream
			InputStream in = new ByteArrayInputStream(stream.toByteArray()); // convert
																				// ByteArrayOutputStream
																				// to
																				// ByteArrayInputStream

			DefaultHttpClient httpclient = new DefaultHttpClient();
			try {
				HttpPost httppost = new HttpPost(IMAGE_UPLOAD_URL); // server
				MultipartEntity reqEntity = new MultipartEntity();
				reqEntity.addPart("myFile", userId + "-" + mEditText.getText() + ".jpg", in);
				//reqEntity.addPart("name", "Uploaded Spot by me. The Spot Man");
				httppost.setEntity(reqEntity);
				Log.i(TAG, "request " + httppost.getRequestLine());
				HttpResponse response = null;
				try {
					response = httpclient.execute(httppost);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					if (response != null)
						Log.i(TAG, "response " + response.getStatusLine().toString());
				} finally {

				}
			} finally {

			}

			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			Toast.makeText(getActivity(), "Uploaded", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.i(TAG, "onResume: " + this);
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		Log.i(TAG, "onSaveInstanceState");
	}

	String mCurrentPhotoPath;
	static final int REQUEST_TAKE_PHOTO = 1;
	static final int REQUEST_GET_PHOTO = 2;
	static 
	File photoFile = null;

	private File createImageFile() throws IOException {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "JPEG_" + timeStamp + "_";
		String storageDir = Environment.getExternalStorageDirectory()+ "/picupload";
		File dir = new File(storageDir);
		if (!dir.exists())
			dir.mkdir();
		File image = new File(storageDir + "/" + imageFileName + ".jpg");
		mCurrentPhotoPath = image.getAbsolutePath();
		Log.i(TAG, "photo path = " + mCurrentPhotoPath);
		return image;
	}
	
	private void setPicGallery(Uri selectedImage){
		Bitmap bitmap = null;
		try {
			bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mImageView.setImageBitmap(bitmap);
		mImageToUpload = bitmap;
	}
	
	private void setPicCamera() {
		// Get the dimensions of the View
		int targetW = mImageView.getWidth();
		int targetH = mImageView.getHeight();

		// Get the dimensions of the bitmap
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		// Determine how much to scale down the image
		int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

		// Decode the image file into a Bitmap sized to fill the View
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor << 1;
		bmOptions.inPurgeable = true;

		Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

		Matrix mtx = new Matrix();
		//mtx.postRotate(180);
		// Rotating Bitmap
		Bitmap rotatedBMP = Bitmap.createBitmap(bitmap, 0, 0,bitmap.getWidth(), bitmap.getHeight(), mtx, true);

		if (rotatedBMP != bitmap)
			bitmap.recycle();

		mImageView.setImageBitmap(rotatedBMP);
		mImageToUpload = rotatedBMP;
	}
}
