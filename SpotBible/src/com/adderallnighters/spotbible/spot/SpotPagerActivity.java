package com.adderallnighters.spotbible.spot;

import java.util.List;
import java.util.UUID;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.Spot;
import com.adderallnighters.spotbible.model.SpotManager;

public class SpotPagerActivity extends FragmentActivity {

	private ViewPager mViewPager;
	private List<Spot> mSpots;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mViewPager = new ViewPager(this);
		this.mViewPager.setId(R.id.viewPager);
		setContentView(mViewPager);
		this.mSpots = SpotManager.get(this).getSpots();
		FragmentManager manager = this.getSupportFragmentManager();
		mViewPager.setAdapter(new FragmentStatePagerAdapter(manager)
		{

			@Override
			public Fragment getItem(int pos) {
				Spot s = mSpots.get(pos);
				return SpotDetailsFragment.newInstance(s.getId());
			}

			@Override
			public int getCount() {
				return mSpots.size();
			}
		});
		
		UUID spotId = (UUID) getIntent().getSerializableExtra(SpotDetailsFragment.EXTRA_SPOT_ID);
		for(int i=0;i<mSpots.size(); i++) {
			if(mSpots.get(i).getId().equals(spotId)){
				mViewPager.setCurrentItem(i);
				break;
			}
		}
		
		
	}
	
}
