package com.adderallnighters.spotbible.model;

import java.util.UUID;

public class Crew {

	private String name;
	private UUID mId;
	
	public Crew(String name){
		this.name = name;
		this.mId = UUID.randomUUID();
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public UUID getId(){
		return mId;
	}
}
