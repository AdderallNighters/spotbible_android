package com.adderallnighters.spotbible.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Image {

	private static final String JSON_FILENAME = "filename";
	private String mFilename;
	
	public Image(String filename) {
		this.mFilename = filename;
	}
	
	public Image(JSONObject json) throws JSONException {
		this.mFilename = json.getString(JSON_FILENAME);
	}
	
	public JSONObject toJSON() throws JSONException
	{
		JSONObject json = new JSONObject();
		json.put(JSON_FILENAME, this.mFilename);
		return json;
	}
	
	public String getFilename()
	{
		return this.mFilename;
	}
}
