package com.adderallnighters.spotbible.model;

import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

public class Spot {

	private static final String TAG = Spot.class.getSimpleName();
	private static final String JSON_IMAGE="image";
	private static final String JSON_NAME="name";
	private static final String JSON_UPLOADED_BY = "uploaded by";
	
	private UUID mId;
	private String mName;
	private String thumbnailURL;
	private Image mImage;
	
	public Spot()
	{
		this.mId = UUID.randomUUID();
	}
	
	public Spot(JSONObject json) throws JSONException
	{
		if(json.has(JSON_IMAGE))
			mImage = new Image(json.getJSONObject(JSON_IMAGE));
	}

	public JSONObject toJSON() throws JSONException{
		JSONObject json = new JSONObject();
		json.put(JSON_NAME, mName);
		json.put(JSON_UPLOADED_BY, "");
		return json;
	}
	
	public UUID getId() {
		return this.mId;
	}

	public void setName(String name) {
		this.mName = name;
	}
	
	public String getName()
	{
		return this.mName;
	}
	
	public String getThumbnailURL() {
		return thumbnailURL;
	}

	public void setThumbnailURL(String thumbnailURL) {
		this.thumbnailURL = thumbnailURL;
	}

	public Image getImage()
	{
		return this.mImage;
	}
	
	public void setImage(Image image)
	{
		this.mImage = image;
	}
	@Override
	public String toString() {
		return mName;
	}
	
	
}
