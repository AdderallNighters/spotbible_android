package com.adderallnighters.spotbible.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

import com.adderallnighters.spotbible.utils.SpotBibleJSONSerializer;

public class SpotManager {

	private static final String TAG = SpotManager.class.getSimpleName();
	private static SpotManager sSpotLab;
	private SpotBibleJSONSerializer mSerializer;
	private List<Spot> mSpots;
	
	private SpotManager(Context appContext)
	{
		this.mSpots = new ArrayList<Spot>();
		
		//request the spots from the server
//		this.mSerializer = new SpotBibleJSONSerializer();
//		try{
//			mSpots = mSerializer.loadSpots();
//		} catch(Exception e){
//			mSpots = new ArrayList<Spot>();
//			Log.e(TAG, "Error loading the spots");
//		}
	}
	
	public void addSpot(Spot s){
		mSpots.add(s);
	}
	
	public List<Spot> getSpots()
	{
		return this.mSpots;
	}
		
	public Spot getSpot(UUID id)
	{
		for(Spot s : mSpots) {
			if(s.getId().equals(id))
				return s;
		}
		return null;
	}
	
	public boolean saveSpot(Spot s){
		try{
			mSerializer.saveSpot(s);
			Log.d(TAG, "Spots saved to server");
			return true;
		} catch(Exception e){
			Log.e(TAG, "Error saving spots " + e);
			return false;
		}
	}
	
	public static SpotManager get(Context c)
	{
		if(sSpotLab != null)
			return sSpotLab;
		else sSpotLab = new SpotManager(c.getApplicationContext());
		return sSpotLab;
	}
	
	
}
