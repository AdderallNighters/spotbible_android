package com.adderallnighters.spotbible.model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.content.Context;
import android.util.Log;

import com.adderallnighters.spotbible.utils.SpotBibleJSONSerializer;
import com.facebook.model.GraphUser;

public class FriendManager {

	private static final String TAG = FriendManager.class.getSimpleName();
	private static FriendManager sFriendLab;
	private List<GraphUser> mNewFriends;
	private SpotBibleJSONSerializer mSerializer;
	
	private FriendManager(Context appContext)
	{
		this.mSerializer = new SpotBibleJSONSerializer(appContext);
		try{
			this.mNewFriends = mSerializer.loadFriends();	
			Log.d(TAG, "Looking for an existing friends list on the server");
		} catch(Exception e){
			this.mNewFriends = new ArrayList<GraphUser>();
			Log.e(TAG, "Error loading the friends from the server");
		}
	}
	
	public List<GraphUser> getFriends(){
		return this.mNewFriends;
	}
		
	public void addFriend(GraphUser friend){
		mNewFriends.add(friend);
	}
	
	public boolean saveFriends(String userId){
		try{
			mSerializer.saveFriends(mNewFriends, userId);
			Log.d(TAG, "Friends saved to the server");
			return true;
		}
		catch(Exception e){
			Log.d(TAG, "Error saving friends");
			return false;
		}
	}
	
	public GraphUser getFriend(UUID id)
	{
		for(GraphUser gu : mNewFriends) {
			if(gu.getId().equals(id))
				return gu;
		}
		return null;
	}
	
	public static FriendManager get(Context c)
	{
		if(sFriendLab != null)
			return sFriendLab;
		else sFriendLab = new FriendManager(c.getApplicationContext());
		return sFriendLab;
	}
	
	
}
