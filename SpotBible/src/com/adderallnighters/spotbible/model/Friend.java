package com.adderallnighters.spotbible.model;

import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.model.GraphUser;

public class Friend {

	private static final String JSON_NAME="name";
	private static final String JSON_ID="id";
	
	private GraphUser mUser;
	private UUID mId;

	public Friend(GraphUser user){
		this.mUser = user;
	}
	
	public UUID getId() {
		return mId;
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject json = new JSONObject();
		json.put(JSON_NAME, mUser.getName());
		json.put(JSON_ID, mUser.getId());
		return json;
	}
}
