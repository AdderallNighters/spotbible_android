package com.adderallnighters.spotbible.feed;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.Spot;
import com.adderallnighters.spotbible.utils.ImageQueueUtils;
import com.android.volley.toolbox.NetworkImageView;

public class FeedListAdapter extends BaseAdapter
{
	private Activity mActivity;
	private List<Spot> mSpots;
	
	public FeedListAdapter(Activity activity, List<Spot> spots){
		this.mActivity = activity;
		this.mSpots = spots;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if(convertView == null)
			convertView = mActivity.getLayoutInflater().inflate(R.layout.list_row_spot, null);
		
		NetworkImageView thumbnail = (NetworkImageView) convertView.findViewById(R.id.thumbnail);
		Spot s = getItem(position);
		TextView nameView = (TextView) convertView.findViewById(R.id.spot_list_item_nameTextValue);
		
		thumbnail.setImageUrl(s.getThumbnailURL(), ImageQueueUtils.getInstance(mActivity.getApplicationContext()).getImageLoader());
		nameView.setText(s.getName());
		return convertView;
	}

	@Override
	public int getCount() {
		return this.mSpots.size();
	}

	@Override
	public Spot getItem(int position) {
		return mSpots.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
}
