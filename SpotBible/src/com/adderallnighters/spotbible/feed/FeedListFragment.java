package com.adderallnighters.spotbible.feed;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.Spot;
import com.adderallnighters.spotbible.spot.SpotDetailsFragment;
import com.adderallnighters.spotbible.spot.SpotPagerActivity;
import com.adderallnighters.spotbible.utils.ImageQueueUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;


public class FeedListFragment extends ListFragment {

	private List<Spot> mSpots;
	private static final String TAG = FeedListFragment.class.getSimpleName();
	private ProgressDialog pDialog;
	private String userId;
		
	public FeedListFragment(String userId){
		this.userId = userId;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getActivity().setTitle(R.string.spots_title);
		//this.mSpots = SpotManager.get(getActivity()).getSpots();
		this.mSpots = new ArrayList<Spot>();
		final FeedListAdapter adapter = new FeedListAdapter(getActivity(), mSpots);
		setListAdapter(adapter);
		
		this.pDialog = new ProgressDialog(getActivity());
		//Start making a feed request
		this.pDialog.setMessage("Loading...");
		this.pDialog.show();
		
		//Creating volley request obj
        JsonArrayRequest spotReq = new JsonArrayRequest(
        		"http://li973-66.members.linode.com/" + userId + "/feed",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();
 
                        // Parsing json
                        for (int i = response.length() - 1; i >= 0; i--) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Spot spot = new Spot();
                                spot.setName(obj.getString("title"));
                                spot.setThumbnailURL(obj.getString("image_url"));
                                mSpots.add(spot);
 
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    	 VolleyLog.d(TAG, "Error: " + error.getMessage());
                        hidePDialog();
                    }
                });	
        ImageQueueUtils.getInstance(getActivity().getApplicationContext()).addToRequestQueue(spotReq);
	}
	
    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id)
	{
		Spot s = ((FeedListAdapter) getListAdapter()).getItem(position);
		Intent i = new Intent(getActivity(), SpotPagerActivity.class);
		i.putExtra(SpotDetailsFragment.EXTRA_SPOT_ID, s.getId());
		startActivity(i);
	}	
}
