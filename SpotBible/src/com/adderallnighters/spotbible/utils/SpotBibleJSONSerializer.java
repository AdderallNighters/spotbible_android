package com.adderallnighters.spotbible.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.adderallnighters.spotbible.model.Spot;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.model.GraphUser;

public class SpotBibleJSONSerializer {

	private static final String TAG = SpotBibleJSONSerializer.class
			.getSimpleName();
	private static final String IMAGE_UPLOAD_URL = "http://li973-66.members.linode.com/friends";
	private Context mContext;
	
	public SpotBibleJSONSerializer(Context context){
		this.mContext = context;	
	}
	
	
	public void saveFriends(List<GraphUser> friends, String userId) throws JSONException {
		// JSONArray jsonArray = new JSONArray();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("my_id", userId);
//		
//		int i=0;
//		for (GraphUser gu : friends) {
		Log.d(TAG, "Friend im adding is " + friends);
		params.put("friend", friends.get(0).getId());
//			i++;
//		}
		
		JSONObject fucker = new JSONObject(params);
		Log.d(TAG, fucker.toString());
		JsonObjectRequest request = new JsonObjectRequest(Method.POST,
				IMAGE_UPLOAD_URL, fucker, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject arg0) {
						VolleyLog.d(TAG, "Response received: " + arg0);
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error Thrown: " + error.getMessage());
					}
				});
		ImageQueueUtils.getInstance(mContext.getApplicationContext())
				.addToRequestQueue(request);

	}

	public void saveSpot(Spot s) throws JSONException {
		JSONObject jsonObject = s.toJSON();
		// send this json to my server
	}

	public List<GraphUser> loadFriends() {
		List<GraphUser> friends = new ArrayList<GraphUser>();
		// get the friends from the server and load that shit up
		return friends;
	}

	public List<Spot> loadSpots() {
		return null;
	}
}
