package com.adderallnighters.spotbible.utils;

import android.content.Context;

import com.adderallnighters.spotbible.R;

public enum Swappable {

	CREW(0),
	FEED(1),
	ADD_SPOT(2),
	SETTINGS(3);
	
	private int index;
	
	private Swappable(int index){
		this.index = index;
	}
	
	public int getIndex(){
		return this.index;
	}
	
	public static Swappable convertToSwappable(Context context, CharSequence s){
		if(s.equals(context.getResources().getString(R.string.settings)))
			return SETTINGS;
		else if(s.equals(context.getResources().getString(R.string.feed)))
			return FEED;
		else if(s.equals(context.getResources().getString(R.string.add_spot)))
			return ADD_SPOT;	
		else return CREW;
	}
}
