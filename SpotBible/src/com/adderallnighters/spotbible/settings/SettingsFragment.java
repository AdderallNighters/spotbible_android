package com.adderallnighters.spotbible.settings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adderallnighters.spotbible.R;
import com.facebook.widget.ProfilePictureView;

public class SettingsFragment extends Fragment {


	private ProfilePictureView profilePicture;
	private TextView username;
	private String userId;
	
	public SettingsFragment(String userId){
		this.userId = userId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_settings, container,false);
		profilePicture = (ProfilePictureView) view.findViewById(R.id.image_profile_pic);
		profilePicture.setCropped(true);
		username = (TextView) view.findViewById(R.id.text_user_name);
		return view;
	}
}
