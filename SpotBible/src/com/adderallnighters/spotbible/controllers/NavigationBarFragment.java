package com.adderallnighters.spotbible.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.mainactivity.MainDisplayActivity;
import com.adderallnighters.spotbible.utils.Swappable;

public class NavigationBarFragment extends Fragment {

	private static final String TAG = NavigationBarFragment.class
			.getSimpleName();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_navigation_bar, parent,false);
		NavigationButtonChangeListener listener = new NavigationButtonChangeListener();

		RadioButton mCrewButton, mNewSpotButton, mFeedButton, mSettingsButton;
		mCrewButton = (RadioButton) view.findViewById(R.id.button_crew);
		mCrewButton.setOnCheckedChangeListener(listener);
		mFeedButton = (RadioButton) view.findViewById(R.id.button_feed);
		mFeedButton.setOnCheckedChangeListener(listener);
		mFeedButton.setChecked(true);
		mNewSpotButton = (RadioButton) view.findViewById(R.id.button_new_spot);
		mNewSpotButton.setOnCheckedChangeListener(listener);
		mSettingsButton = (RadioButton) view.findViewById(R.id.button_settings);
		mSettingsButton.setOnCheckedChangeListener(listener);

		return view;
	}

	private class NavigationButtonChangeListener implements
			OnCheckedChangeListener {

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			Activity parentActivity = getActivity();
			if (isChecked && parentActivity instanceof MainDisplayActivity) {
				Swappable newFragment = Swappable.convertToSwappable(
						parentActivity, (String) buttonView.getText());
				((MainDisplayActivity) getActivity()).showFragment(newFragment,
						false);
			}

		}

	}

}
