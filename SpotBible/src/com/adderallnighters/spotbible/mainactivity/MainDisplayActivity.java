package com.adderallnighters.spotbible.mainactivity;

import java.util.Set;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.controllers.NavigationBarFragment;
import com.adderallnighters.spotbible.crew.CrewListFragment;
import com.adderallnighters.spotbible.feed.FeedListFragment;
import com.adderallnighters.spotbible.settings.SettingsFragment;
import com.adderallnighters.spotbible.spot.AddSpotFragment;
import com.adderallnighters.spotbible.spot.AddSpotFragment;
import com.adderallnighters.spotbible.utils.Swappable;

public class MainDisplayActivity extends FragmentActivity {
	
	private static final int REAUTH_ACTIVITY_CODE = 100; 
	private static final String TAG = MainDisplayActivity.class.getSimpleName();
	private Fragment[] swapFragments = new Fragment[Swappable.values().length];
	private String userId;
		
	@Override 
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.general_activity_fragment);
		String userId = getIntent().getExtras().getString("ID");
		FragmentManager manager = getSupportFragmentManager();
		NavigationBarFragment navBar = new NavigationBarFragment();
		swapFragments[Swappable.CREW.getIndex()] = new CrewListFragment(userId);
		swapFragments[Swappable.FEED.getIndex()] = new FeedListFragment(userId);
		swapFragments[Swappable.ADD_SPOT.getIndex()] = new AddSpotFragment(userId);
		swapFragments[Swappable.SETTINGS.getIndex()] = new SettingsFragment(userId);
		
		FragmentTransaction transaction = manager.beginTransaction();	
		transaction.replace(R.id.navigation_container_general, navBar);
		transaction.commit();
		
	}
	
	public void showFragment(Swappable newFragment, boolean addToBackStack){
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		Log.d(TAG, "In show fragment with my dude " + newFragment);
		for(Swappable s : Swappable.values()){
			if(s == newFragment)
				transaction.replace(R.id.display_container_general, swapFragments[s.getIndex()]);
		}
		if(addToBackStack)
			transaction.addToBackStack(null);
		transaction.commit();	
	}

}
