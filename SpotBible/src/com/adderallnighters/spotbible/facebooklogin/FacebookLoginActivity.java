package com.adderallnighters.spotbible.facebooklogin;

import android.support.v4.app.Fragment;

import com.adderallnighters.spotbible.controllers.SingleFragmentActivity;

public class FacebookLoginActivity extends SingleFragmentActivity {

	@Override
	protected Fragment createFragment() {
		return new FacebookLoginFragment();
	}


}
