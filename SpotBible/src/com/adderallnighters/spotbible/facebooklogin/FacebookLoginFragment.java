package com.adderallnighters.spotbible.facebooklogin;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.mainactivity.MainDisplayActivity;
import com.facebook.AppEventsLogger;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

public class FacebookLoginFragment extends Fragment {

	private static final String UPLOAD_URL = "http://li973-66.members.linode.com/text";
	private static final String TAG = FacebookLoginFragment.class
			.getSimpleName();
	private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = new SessionStatusCallback();
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// This maintains the Facebook login session
		this.uiHelper = new UiLifecycleHelper(getActivity(), callback);
		this.uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_facebook_login, parent,
				false);
		TextView text = (TextView) view.findViewById(R.id.textview_app_name);
		Typeface customFont = Typeface.createFromAsset(getActivity()
				.getAssets(), "fonts/SpotBible.ttf");
		text.setTypeface(customFont);

		LoginButton authButton = (LoginButton) view
				.findViewById(R.id.button_facebook_login);
		authButton.setReadPermissions("user_friends");
		authButton.setFragment(this);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		uiHelper.onResume();
		AppEventsLogger.activateApp(getActivity());
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
		AppEventsLogger.deactivateApp(getActivity());
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		Log.i(TAG, "An Activity Result");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		uiHelper.onSaveInstanceState(savedInstanceState);
	}

	private void onSessionStateChanged(final Session session,SessionState state, Exception exception) {
		final Intent i = new Intent(getActivity(), MainDisplayActivity.class);
		if (state.isOpened()) {
			
			Log.d(FacebookLoginActivity.class.getSimpleName(),"Logged in...Starting the app");
			Request request = Request.newMeRequest(session,
					new Request.GraphUserCallback() {
						@Override
						public void onCompleted(GraphUser user,
								Response response) {
							// If the response is successful
							if (session == Session.getActiveSession()) {
								if (user != null) {
									i.putExtra("ID", user.getId());
									i.putExtra("USERNAME", user.getName());
									Log.d(TAG, user.getId());
									getActivity().startActivity(i);
								}
							}
							if (response.getError() != null) {
							}
						}
					});

			request.executeAsync();

		} else
			Log.d(FacebookLoginActivity.class.getSimpleName(), "Logged out...");
	}

	private class SessionStatusCallback implements Session.StatusCallback {

		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChanged(session, state, exception);
		}
	}
}
