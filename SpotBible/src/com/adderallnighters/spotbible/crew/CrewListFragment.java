package com.adderallnighters.spotbible.crew;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.Friend;
import com.adderallnighters.spotbible.model.FriendManager;
import com.adderallnighters.spotbible.utils.ImageQueueUtils;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.facebook.model.GraphUser;

public class CrewListFragment extends ListFragment {

	private static final String TAG = CrewListFragment.class.getSimpleName();
	private static final String url = "http://spotbible.herokuapp.com";
	private Button mAddFriends;
	private List<GraphUser> mMembers;
	private CrewMemberListAdapter mAdapter;
	private ProgressDialog pDialog;
	private String userId;
	
	public CrewListFragment(String userId){
		this.userId = userId;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().setTitle(R.string.spots_title);
		// this.mMembers = FriendManager.get(getActivity()).getFriends();
		this.mMembers = new ArrayList<GraphUser>();
		mAdapter = new CrewMemberListAdapter(getActivity(), mMembers);
		this.setListAdapter(mAdapter);

		pDialog = new ProgressDialog(getActivity());
		// Showing progress dialog before making http request
		pDialog.setMessage("Loading...");
		pDialog.show();

		// Creating volley request obj
		JsonArrayRequest spotReq = new JsonArrayRequest(url,
				new Response.Listener<JSONArray>() {
					@Override
					public void onResponse(JSONArray response) {
						Log.d(TAG, response.toString());
						hidePDialog();

						// Parsing json
						for (int i = 0; i < response.length(); i++) {
							try {
								JSONObject obj = response.getJSONObject(i);
//								Spot spot = new Spot();
								//Friend f = new Friend();
//								spot.setName(obj.getString("title"));
//								spot.setThumbnailURL(obj.getString("image_url"));
//								mSpots.add(spot);

							} catch (JSONException e) {
								e.printStackTrace();
							}
						}
						mAdapter.notifyDataSetChanged();
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						VolleyLog.d(TAG, "Error: " + error.getMessage());
						hidePDialog();

					}
				});
		ImageQueueUtils.getInstance(getActivity().getApplicationContext())
				.addToRequestQueue(spotReq);

	}

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_crew, null);
		this.mAddFriends = (Button) v.findViewById(R.id.button_add_friends);
		mAddFriends.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(),AddFacebookFriendsActivity.class);
				startActivity(i);
			}
		});

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.d(TAG, "Resuming. Notify that I have new friends");
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.d(TAG, "On pause of the crew list fragment");
		FriendManager.get(getActivity()).saveFriends(userId);
	}

}
