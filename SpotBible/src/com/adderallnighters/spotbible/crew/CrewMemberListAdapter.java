package com.adderallnighters.spotbible.crew;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.FriendManager;
import com.adderallnighters.spotbible.model.Spot;
import com.adderallnighters.spotbible.utils.ImageQueueUtils;
import com.android.volley.toolbox.NetworkImageView;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

public class CrewMemberListAdapter extends BaseAdapter {

	private List<GraphUser> mFriends;
	private Activity mActivity;
	
	public CrewMemberListAdapter(Activity activity, List<GraphUser> friends){
		this.mActivity = activity;
		this.mFriends = friends;
	}
	
	@Override
	public int getCount() {
		return mFriends.size();
	}

	@Override
	public GraphUser getItem(int position) {
		return mFriends.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = mActivity.getLayoutInflater().inflate(R.layout.list_row_members, null);
		
		TextView text = (TextView) convertView.findViewById(R.id.crew_list_item_nameTextView);
		ProfilePictureView pic = (ProfilePictureView) convertView.findViewById(R.id.image_profile_pic);
		pic.setCropped(true);
		pic.setProfileId(getItem(position).getId());
		text.setText(getItem(position).getName());
		Button add = (Button) convertView.findViewById(R.id.button_remove_friend);
		add.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				((Button)v).setText("Removed"); 
				//mFriendManager.addFriend(getItem(position));
				//Log.d(TAG, "You are now friends with..." + getItem(position).getName());
			}
		});
		return convertView;
	}

}
