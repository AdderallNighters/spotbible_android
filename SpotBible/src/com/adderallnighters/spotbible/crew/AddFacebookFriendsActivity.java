package com.adderallnighters.spotbible.crew;

import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.FriendManager;
import com.facebook.Request;
import com.facebook.Request.GraphUserListCallback;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphUser;

public class AddFacebookFriendsActivity extends ListActivity {

	private static final String TAG = AddFacebookFriendsActivity.class.getSimpleName();
	private AddFriendListAdapter mAdapter;
	private FriendManager mFriendManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle(R.string.spots_title);

		this.mFriendManager = FriendManager.get(this);
		Session activeSession = Session.getActiveSession();
		if (activeSession.getState().isOpened()) {
			Request friendRequest = Request.newMyFriendsRequest(activeSession,
					new GraphUserListCallback() {
						@Override
						public void onCompleted(List<GraphUser> users,
								Response response) {
							Log.d(TAG, response.toString());
							Log.d(TAG, users.toString());
							mAdapter = new AddFriendListAdapter(AddFacebookFriendsActivity.this, users);
							setListAdapter(mAdapter);
						}
					});
			Bundle params = new Bundle();
			params.putString("fields", "id,name,picture");
			friendRequest.setParameters(params);
			friendRequest.executeAsync();
		}
	}
	
	@Override
	public void onPause(){
		super.onPause();
		Log.d(TAG, "This is the on pause called. not that other guy");
		//mFriendManager.saveFriends();
	}
}