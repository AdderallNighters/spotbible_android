package com.adderallnighters.spotbible.crew;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.adderallnighters.spotbible.R;
import com.adderallnighters.spotbible.model.FriendManager;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

public class AddFriendListAdapter extends BaseAdapter{

	private static final String TAG = AddFriendListAdapter.class.getSimpleName();
	private List<GraphUser> mFriends;
	private Activity mActivity;
	private FriendManager mFriendManager;
	
	public AddFriendListAdapter(Activity activity, List<GraphUser> friends){
		this.mFriends = friends;
		this.mActivity = activity;
		this.mFriendManager = FriendManager.get(activity);
	}
	
	@Override
	public int getCount() {
		return mFriends.size();
	}

	@Override
	public GraphUser getItem(int position) {
		return mFriends.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = mActivity.getLayoutInflater().inflate(R.layout.list_row_crew, null);
		
		TextView text = (TextView) convertView.findViewById(R.id.crew_list_item_nameTextView);
		ProfilePictureView pic = (ProfilePictureView) convertView.findViewById(R.id.image_profile_pic);
		pic.setCropped(true);
		pic.setProfileId(getItem(position).getId());
		text.setText(getItem(position).getName());
		Button add = (Button) convertView.findViewById(R.id.button_add_friend);
		add.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				((Button)v).setText("Added"); 
				((Button)v).setBackgroundColor(Color.GREEN);
				mFriendManager.addFriend(getItem(position));
				Log.d(TAG, "You are now friends with..." + getItem(position).getName());
			}
		});
		return convertView;
	}

	//TODO on destroy needs to save the new relationships
	
}
